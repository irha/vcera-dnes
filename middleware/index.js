var Trip          = require("../models/trip");
var Comment       = require("../models/comment");
var middlewareObj = {};
// =============================================================================
// OWNERSHIP OF TRIP
// =============================================================================
middlewareObj.checkTripOwnership = function(req, res, next){
  if(req.isAuthenticated()){
    Trip.findById(req.params.id, function(err, foundTrip){
      if(err){
        req.flash("error", "Trip not found");
        res.redirect("/trips");
      } else {
        if(foundTrip.author.id.equals(req.user._id) || req.user.admin){
          next();
        } else {
          req.flash("error", "You don't have permission to do that!");
          res.redirect("back");
        }
      }
    });
  } else {
    req.flash("error", "You need to be logged in to do that!");
    res.redirect("back");
  }
};
// =============================================================================
// OWNERSHIP OF COMMENT
// =============================================================================
middlewareObj.checkCommentOwnership = function(req, res, next){
  if(req.isAuthenticated()){
    Comment.findById(req.params.comment_id, function(err, foundComment){
      if(err){
        res.redirect("back");
      } else {
        if(foundComment.author.id.equals(req.user._id) || req.user.admin){
          next();
        } else {
          req.flash("error", "You don't have permission to do that!");
          res.redirect("back");
        }
      }
    });
  } else {
    req.flash("error", "You need to be logged in to do that!");
    res.redirect("back");
  }
};
// =============================================================================
// CHECK IF USER IS LOGGED IN
// =============================================================================
middlewareObj.isLoggedIn = function(req, res, next){
  if(req.isAuthenticated()){
    return next();
  }
  req.flash("error", "You need to be logged in to do that!");
  res.redirect("/login");
};
// =============================================================================
// CHECK IF USER IS ADMIN
// =============================================================================
middlewareObj.isAdmin = function(req, res, next){
  if(req.isAuthenticated()){
    if(req.user.admin)  {
      return next();
    } else {
      req.flash("error", "You are not Admin!");
      res.redirect("/");
    }
  }
  req.flash("error", "You need to be logged in to do that!");
  res.redirect("/login");
};

module.exports = middlewareObj;
