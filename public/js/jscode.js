// =============================================================================
// ZOOM IMAGE IN SHOW PAGE
// =============================================================================
$(".zoom").ready(function(){
  $(".zoom").click(function(){
    var src = $(this).attr("src");
    $("#img01").attr("src", src);
    $("#modal01").css({"display": "block"});
  });
});
// =============================================================================
// ADD DELETE ICON TO INPUT AT PAGE LOAD
// =============================================================================
$(':input').ready(function(){
  $(':input.clearable[type="text"][value!=""]').parent().append('<span id="clear" onClick="fireIt(this)" class="close-icon"></span>');
});
// =============================================================================
// DYNAMICALLY ADD/REMOVE DELETE ICON TO INPUT
// =============================================================================
$(':input.clearable[type="text"]').on("change keyup paste click toggle", function(){
  var new_var = $(this).val();
  $(this).attr("value", new_var);

    if($(this).val().length > 0){
      if ($(this).parent().find("span").length){
          } else {
            $(this).parent().append('<span id="clear" onClick="fireIt(this)" class="close-icon"></span>');
          }
    }   else {
        $(this).parent().find("span").remove("span");
    }

});
// =============================================================================
// REMOVE INPUT AND SPAN WITH BTN ON CLICK
// =============================================================================
function fireIt (elem) {
    $(elem).prev('input').val("");
    $(elem).remove();
}
// =============================================================================
// SHOW PASSWORD
// =============================================================================
$("#show").on("click", function(){
  if($(this).prev().attr("type") == "password"){
    $(this).prev().attr("type", "text");
  }  else {
    $(this).prev().attr("type", "password");
  }
});
// =============================================================================
// VALIDATE NUM OF ADDED IMAGES AND STOP FORM IF THERE IS MORE THAN 5 FILES
// =============================================================================
$(function(){
    $("button[type='submit']").click(function(){
        var $fileUpload = $("input[type='file']");
        if (parseInt($fileUpload.get(0).files.length)>5){
         alert("You can upload a maximum of 5 files and you've selected " + $fileUpload.get(0).files.length + " files to upload");
         return false;
        }
    });
});
// =============================================================================
// NOT ALLOW ANY CHARACTERS IN NUMBER INPUT
// =============================================================================
function FilterInput(event) {
    var keyCode = ('which' in event) ? event.which : event.keyCode;
    isNotWanted = (keyCode == 69);
    return !isNotWanted;
}

function handlePaste (e) {
    var clipboardData, pastedData;

    // Get pasted data via clipboard API
    clipboardData = e.clipboardData || window.clipboardData;
    pastedData = clipboardData.getData('Text').toUpperCase();

    if(pastedData.indexOf('E')>-1) {
        //alert('found an E');
        e.stopPropagation();
        e.preventDefault();
    }
}
// =============================================================================
$('#video').on('keyup paste click', function()  {
  var str = $(this).val();
  var newStr = str.replace('watch?v=', 'embed/');
  $(this).attr('value', newStr);
});
