require('dotenv').config();
var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    mongoose        = require("mongoose"),
    Trip            = require("./models/trip"),
    Comment         = require("./models/comment"),
    passport        = require("passport"),
    LocalStrategy   = require("passport-local"),
    User            = require("./models/user"),
    methodOverride  = require("method-override"),
    flash           = require("connect-flash");

//requring routes
var commentRoutes     = require("./routes/comments"),
    tripRoutes        = require("./routes/trips"),
    indexRoutes       = require("./routes/index");
    userRoutes        = require("./routes/users");
var url = process.env.DATABASEURL;
// var url = "mongodb://localhost/trip";
mongoose.connect(url);

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));
app.use(flash());

app.locals.moment = require('moment');
//passport config
app.use(require("express-session")({
  secret: process.env.PASSPORT_SECRET_KEY,
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//pass user to every template
app.use(function(req, res, next){
  res.locals.currentUser  = req.user;
  res.locals.error        = req.flash("error");
  res.locals.success      = req.flash("success");
  next();
});

app.use("/", indexRoutes);
app.use("/trips", tripRoutes);
app.use("/trips/:id/comments", commentRoutes);
app.use("/users", userRoutes);

app.listen(process.env.PORT || 3000, function(){
    console.log("Server running!");
});
