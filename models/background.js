var mongoose  = require('mongoose');

var backSchema  = mongoose.Schema({
  landing: String,
  pages: String
});

module.exports = mongoose.model('Backgrounds', backSchema);
