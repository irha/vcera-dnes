var mongoose = require("mongoose");

var tripSchema = new mongoose.Schema({
   name: String,
   price: Number,
   location: String,
   lat: Number,
   lng: Number,
   images: Array,
   description: String,
   video: String,
   created: {type: Date, default: Date.now},
   author: {
    id: {
       type: mongoose.Schema.Types.ObjectId,
       ref: "User"
    },
    username: String
   },
   comments: [
      {
         type: mongoose.Schema.Types.ObjectId,
         ref: "Comment",
         created: {type: Date, default: Date.now}
      }
   ]
});

module.exports = mongoose.model("Trip", tripSchema);
