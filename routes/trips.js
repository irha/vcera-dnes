var express     = require("express");
var router      = express.Router();
var Trip        = require("../models/trip");
var Comment     = require("../models/comment");
var middleware  = require("../middleware");
var geocoder    = require('geocoder');
var mongoose    = require("mongoose");
var multer      = require("multer");
var aws         = require("aws-sdk");
var multerS3    = require("multer-s3");
var fs          = require("fs");
// =============================================================================
// UPLOAD TO S3
// =============================================================================
aws.config.update({
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    region: 'eu-central-1'
});

var s3 = new aws.S3();

var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: process.env.S3_BUCKET_TRIP,
        key: function (req, file, cb) {
            cb(null, Date.now() + "&" + req.user.username + "&" + req.body.name + "&" + file.originalname);
        }
    })
});
// =============================================================================
// INDEX
// =============================================================================
router.get("/", function(req, res){
    Trip.find({}).sort({'created': -1}).exec(function(err, allTrips){
       if(err){
           console.log(err);
       } else {
          res.render("trips/index", {trips: allTrips, page: 'trips'});
       }
    });
});
// =============================================================================
// CREATE
// =============================================================================
router.post("/", middleware.isLoggedIn, upload.array('images', 5), function(req, res){
  //upload links to DB
  var filepath = [];

  if(typeof req.files !== "undefined") {
          for(var i = 0; i < req.files.length; i++) {
              filepath.push(req.files[i].key);
          }
      } else {
          filepath.push("https://s3.eu-central-1.amazonaws.com/radik-trip-pictures/no-image.png");
        }

    var name = req.body.name;
    var price = req.body.price;
    var description = req.body.description;
    var author = {
      id: req.user._id,
      username: req.user.username
    };
    var video = req.params.video;
    geocoder.geocode(req.body.location, function (err, data) {
      var lat = data.results[0].geometry.location.lat;
      var lng = data.results[0].geometry.location.lng;
      var location = data.results[0].formatted_address;
      var newTrip = {name: name, price: price, images: filepath, video: video, description: description, author: author, location: location, lat: lat, lng: lng};
    Trip.create(newTrip, function(err, newAdd){
      if(err){
        console.log("ERROR while saving!");
      } else {
        res.redirect("/trips");
        }
        });
      });
});
// =============================================================================
// NEW
// =============================================================================
router.get("/new", middleware.isLoggedIn, function(req, res){
    res.render("trips/new", {page: 'new'});
});
// =============================================================================
// SHOW
// =============================================================================
router.get("/:id", function(req, res){
  Trip.findById(req.params.id).populate({path: 'comments', options: {sort: {created: -1}}}).exec( function(err, foundTrip){
    if(err){
      console.log("ERROR!" + err);
    } else {
      res.render("trips/show", {trip: foundTrip});
    }
  });
});
// =============================================================================
// EDIT
// =============================================================================
router.get("/:id/edit", middleware.checkTripOwnership, function(req, res){
  Trip.findById(req.params.id, function(err, foundTrip){
    res.render("trips/edit", {trip: foundTrip});
  });
});
// =============================================================================
// UPDATE
// =============================================================================
router.put("/:id", middleware.checkTripOwnership, upload.array("images",5), function(req, res){
  const newImages = [];
  if(req.files.length) {
    for(var i = 0; i < req.files.length; i++) {
        newImages.push(req.files[i].key);
    }
  }
  var name = req.body.name;
  var price = req.body.price;
  var video = req.body.video;
  var description = req.body.description;
  var updateTrip = {name: name, price: price, video: video, description: description, $push: { images: newImages}};
  Trip.findByIdAndUpdate(req.params.id, updateTrip, function(err, updatedTrip){
    if(err){
      res.redirect("/trips");
    } else {

      // if(req.files.length) {
      //   for(var i = 0; i < req.files.length; i++) {
      //       updatedTrip.images.push(req.files[i].key);
      //   }
      //   updatedTrip.save();
      // }
      // if any images have been selected for removal, remove them from images array
      if(req.body.removals && req.body.removals.length) {
        for(var j = 0; j < req.body.removals.length; j++) {
          var index = updatedTrip.images.indexOf(req.body.removals[j]);
          updatedTrip.images.splice(index, 1);
          deleteFiles(req.body.removals);

        }
        updatedTrip.save();

      }
      // if the no-image placeholder exists and other images exist, then remove it
      if(updatedTrip.images.length > 1 && updatedTrip.images.indexOf("dont_remove/addMe.jpg") !== -1) {
        updatedTrip.images.splice("dont_remove/addMe.jpg", 1);
        updatedTrip.save();

      }
      // if no images exist (all have been deleted) then add the no-image placeholder
      if(updatedTrip.images.length === 0){
        updatedTrip.images.push("dont_remove/addMe.jpg");
        updatedTrip.save();

      }
      res.redirect("/trips/" + req.params.id);
      }
    });
});
// =============================================================================
// DESTROY
// =============================================================================
router.delete("/:id", middleware.checkTripOwnership, function(req, res){
  Trip.findByIdAndRemove(req.params.id, function(err, removedTrip){
    if(err){
      res.redirect("/trips");
    } else {
      // console.log("removedTrip: " + removedTrip);
      var comments = removedTrip.comments;
      // console.log("All comments: " + comments);
      if(comments.length !== 0){
        comments.forEach(function(comment){
          Comment.findByIdAndRemove(comment, function(err, removedComment){
            if(err){
              console.log(err);
            }
            console.log("Removed comment: " + removedComment);
          });

        });
      }

      if(removedTrip.images){
        deleteFiles(removedTrip.images);
      }

      req.flash("success", "Trip succesfully deleted!");
      res.redirect("/trips");
    }
  });
});
// =============================================================================
// DELETE FROM AMAZON S3 FUNCTION
// =============================================================================
function deleteFiles(imagePath) {
  var s3 = new aws.S3();
  var objects = [];
  for(var k = 0; k < imagePath.length; k++){
    var img = imagePath[k];
    console.log(img);
    objects.push({Key : img});
  }
    s3.deleteObjects({
        Bucket: process.env.S3_BUCKET_TRIP,
        Delete: {
            Objects: objects
        }
    }, function(err, data) {
        if (err) {
            console.log("Couldn't delete image file: " + err);
        }
        console.log("removed");
    });
}

module.exports = router;
