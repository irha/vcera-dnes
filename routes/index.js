var express     = require("express");
var router      = express.Router();
var passport    = require("passport");
var User        = require("../models/user");
var Trip        = require("../models/trip");
var Background  = require('../models/background');
var async       = require("async");
var nodemailer  = require("nodemailer");
var crypto      = require("crypto");
var multer      = require("multer");
var aws         = require("aws-sdk");
var multerS3    = require("multer-s3");
var fs          = require("fs");
var middleware  = require("../middleware");
// =============================================================================
// UPLOAD TO S3
// =============================================================================
aws.config.update({
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    region: 'eu-central-1'
});

var s3 = new aws.S3();

var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: process.env.S3_BUCKET_USER,
        key: function (req, file, cb) {
            cb(null, Date.now() + "&" + req.body.username + "&" +file.originalname);
        }
    })
});
// =============================================================================
// ROOT ROUTE
// =============================================================================
router.get("/", function(req, res){
    Background.findById('59c17947585aee075125515a', function(err, backgrounds)  {
      if(err) {
        console.log(err);
      } else {
        res.render('landing', {background: backgrounds});
      }
    });
});
// =============================================================================
// REGISTER ROUTE
// =============================================================================
router.get("/register", function(req, res){
   res.render("register", {page: 'register'});
});
// =============================================================================
// SIGN UP LOGIC
// =============================================================================
router.post("/register", upload.single("image"), function(req, res){
  filepath = undefined;

  if(req.file) {
      filepath = req.file.key;
  }

  var username  = req.body.username;
  var firstName = req.body.firstName;
  var lastName  = req.body.lastName;
  var email     = req.body.email;

  var newUser = {username: username, firstName: firstName, lastName: lastName, email: email, avatar: filepath};
  User.register(newUser, req.body.password, function(err, user){
    if(err){
    console.log(err);
    return res.render("register", {error: err.message});
    } else {
      passport.authenticate("local")(req, res, function(){
        req.flash("success", "Welcome " + user.username + "!");
        res.redirect("/trips");
        });
      }
    });
});
// =============================================================================
// LOGIN FORM
// =============================================================================
router.get("/login", function(req, res){
   res.render("login", {page: 'login'});
});
// =============================================================================
// LOGIN LOGIC
// =============================================================================
router.post("/login", passport.authenticate("local",
  {
    successRedirect: "/trips",
    failureRedirect: "/login",
    failureFlash : true
  }), function(req, res){
});
// =============================================================================
// LOGOUT
// =============================================================================
router.get("/logout", function(req, res){
  req.logout();
  req.flash("success", "Logged You out!");
  res.redirect("/trips");
});
// =============================================================================
// FORGOT PASSWORD
// =============================================================================
router.get("/forgot", function(req, res){
  res.render("forgot");
});
// =============================================================================
// SEND MAIL AFTER FORGOT
// =============================================================================
router.post("/forgot", function(req, res){
  async.waterfall([
    function(done)  {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString("hex");
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({email: req.body.email}, function(err, user)  {
        if(!user) {
          req.flash("error", "No account assigned with email");
          return res.redirect("/forgot");
        }

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; //10 minutes

        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      var smtpTransport = nodemailer.createTransport({
        service: "Gmail",
        auth: {
          user: "trip.vcera.dnes@gmail.com",
          pass: process.env.GMAILPW
        }
      });
      var mailOptions = {
        to: user.email,
        from: "trip.vcera.dnes@gmail.com",
        subject: "#Vcera-dnes password reset",
        text: 'You received this mail, because You have requested tho reset of the password.\n\n' +
              'Please click on the following link, or paste this into Your browser to complete the process:\n\n' +
              'http://vcera-dnes.herokuapp.com/reset/' + token + '\n\n' +
              'If You did not request this, please ignore it :) \n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash("success", "An email has been sent to " + user.email + " with further instructions.");
        done(err, "done");
      });
    }
  ], function(err){
    if(err) return next(err);
    res.redirect("/forgot");
  });
});
// =============================================================================
// CREATE NEW PASSWORD
// =============================================================================
router.get("/reset/:token", function(req, res)  {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user)  {
    if(!user) {
      req.flash("error", "Password reset token is invalid or has expired");
      return res.redirect("/forgot");
    }
    res.render("reset", {token: req.params.token});
  });
});
// =============================================================================
// NEW PASSWORD
// =============================================================================
router.post('/reset/:token', function(req, res) {
  async.waterfall([
    function(done) {
      User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('back');
        }
        if(req.body.password === req.body.confirm) {
          user.setPassword(req.body.password, function(err) {
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;

            user.save(function(err) {
              req.logIn(user, function(err) {
                done(err, user);
              });
            });
          });
        } else {
            req.flash("error", "Passwords do not match.");
            return res.redirect('back');
        }
      });
    },
    function(user, done) {
      var smtpTransport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
          user: 'trip.vcera.dnes@gmail.com',
          pass: process.env.GMAILPW
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'trip.vcera.dnes@gmail.com',
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('success', 'Success! Your password has been changed.');
        done(err);
      });
    }
  ], function(err) {
    res.redirect('/campgrounds');
  });
});
// =============================================================================
// ABOUT PAGE
// =============================================================================
router.get("/about", function(req, res){
  res.render("other/about", {page: 'about'});
});
// =============================================================================
// ADMIN PAGE
// =============================================================================
router.get('/admin', middleware.isAdmin, function(req, res) {
  Trip.find({}).exec(function(err, allTrips)  {
    if(err) {
      console.log(err);
    } else {
      User.find({}).exec(function(err, allUsers)  {
        if(err) {
          console.log(err);
          res.redirect('/');
        } else {
          res.render('admin', {trips: allTrips, users: allUsers, page: 'admin'});
        }
      });
    }
  });
});

// =============================================================================
// EDIT landing back
// =============================================================================
router.get('/editLandingBackground', middleware.isAdmin, function(req, res) {
  Background.findById('59c17947585aee075125515a', function(err, backgrounds)  {
    if(err) {
      console.log(err);
    } else {
      res.render('editLanding', {background: backgrounds});
    }
  });
});
// =============================================================================
// update background
// =============================================================================
router.put('/editLanding', middleware.isAdmin, upload.single('landing'), function(req, res) {
  filepath = undefined;
  if(req.file) {
      filepath = req.file.key;
  }
  var updateLanding = {landing: filepath};
  Background.findByIdAndUpdate('59c17947585aee075125515a', updateLanding, function(err, updateLAnding)  {
    res.redirect('/');
  });
});

module.exports = router;
