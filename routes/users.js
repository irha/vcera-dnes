var express     = require("express");
var router      = express.Router({mergeParams: true});
var Trip        = require("../models/trip");
var Comment     = require("../models/comment");
var User        = require("../models/user");
var middleware  = require("../middleware");
var passport    = require("passport");
var multer      = require("multer");
var aws         = require("aws-sdk");
var multerS3    = require("multer-s3");
var fs          = require("fs");
// =============================================================================
// UPLOAD TO S3
// =============================================================================
aws.config.update({
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    region: 'eu-central-1'
});

var s3 = new aws.S3();

var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: process.env.S3_BUCKET_USER,
        key: function (req, file, cb) {
            cb(null, Date.now() + "&" + req.body.username + "&" + file.originalname);
        }
    })
});
// =============================================================================
// SHOW
// =============================================================================
router.get("/:id", function(req, res){
  User.findById(req.params.id, function(err, foundUser) {
    if(err) {
      req.flash("error", "User not found");
      res.redirect("/");
    }
    if(foundUser) {
      Trip.find().where("author.id").equals(foundUser._id).exec(function(err, trips)  {
        if(err) {
          req.flash("error", "Trips not found");
          res.redirect("/");
        } else {
          res.render("users/show", {user: foundUser, trips: trips, page: 'userProfile'});
        }
      });
    } else {
      req.flash('error', "User don't exist yet");
      res.redirect('/trips');
    }
  });
});
// =============================================================================
// EDIT
// =============================================================================
router.get('/:id/edit', middleware.isLoggedIn, function(req, res) {
  User.findById(req.params.id, function(err, foundUser) {
    if(err) {
      console.log(err);
    } else {
      res.render('users/edit', {user: foundUser});
    }
  });
});
// =============================================================================
// UPDATE
// =============================================================================
router.put("/:id", middleware.isAdmin, function(req, res) {
  User.findByIdAndUpdate(req.params.id, req.body.user, function(err, updatedUser) {
    if(err) {
      console.log(err);
    } else {
      console.log(req.body.user);
      updatedUser.save();
      res.redirect('/users/' + req.params.id);
    }
  });
});
// =============================================================================
// EDIT PASSWORD
// =============================================================================
router.get('/:id/editPassword', middleware.isLoggedIn, function(req, res) {
  User.findById(req.params.id, function(err, foundUser) {
    if(err) {
      console.log(err);
    } else {
      res.render('users/editPassword', {user: foundUser});
    }
  });
});
// =============================================================================
// UPDATE NEW PASSWORD
// =============================================================================
router.put("/:id/newPassword", middleware.isLoggedIn, function(req, res) {
  User.findById(req.params.id, function(err, user)  {
    if(!user) {
      req.flash('error', 'User not found');
      res.redirect('back');
    } else {
      if(req.body.password === req.body.confirm) {
        user.setPassword(req.body.password, function(err) {

          user.save();
          req.logout();
          req.flash('success', 'Password changed');
          res.redirect('/');

        });
      } else {
          req.flash("error", "Passwords do not match.");
          return res.redirect('back');
      }
    }
  });
});
// =============================================================================
// EDIT AVATAR
// =============================================================================
router.get('/:id/editAvatar', middleware.isLoggedIn, function(req, res) {
  User.findById(req.params.id, function(err, foundUser) {
    if(err) {
      console.log(err);
    } else {
      res.render('users/editAvatar', {user: foundUser});
    }
  });
});
// =============================================================================
// UPDATE NEW AVATAR
// =============================================================================
router.put("/:id/newAvatar", middleware.isLoggedIn, upload.single('image'), function(req, res) {
  filepath = undefined;
  if(req.file) {
      filepath = req.file.key;
  }

  var updateAvatar = {avatar: filepath};
  console.log(updateAvatar);
  User.findByIdAndUpdate(req.params.id, updateAvatar, function(err, updatedUser) {
    if(updatedUser.avatar)  {
      deleteFile(updatedUser.avatar);
    }
    updatedUser.save();
    req.flash("success", "Avatar changed");
    res.redirect("/users/" + req.params.id);
  });
});
// =============================================================================
// DESTROY
// =============================================================================
router.delete("/:id", middleware.isLoggedIn, function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, removedUser)  {
    deleteFile(removedUser.avatar);
    if(err) {
      req.flash('error', "Can't remove user");
      res.redirect("back");
    } else {
      req.flash('success', 'User removed');
      res.redirect('/');
    }
  });
});
// =============================================================================
// DELETE FROM AMAZON S3 FUNCTION
// =============================================================================
function deleteFile(image) {

  var s3 = new aws.S3();

  var params = {
    Bucket: process.env.S3_BUCKET_USER,
    Key: image
  };
  s3.deleteObject(params, function(err, data) {
      if (data) {
        console.log("removed");
      } else {
        console.log("Couldn't delete image file: " + err);
      }
  });
}

module.exports = router;
