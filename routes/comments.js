var express     = require("express");
var router      = express.Router({mergeParams: true});
var Trip        = require("../models/trip");
var Comment     = require("../models/comment");
var middleware  = require("../middleware");
// =============================================================================
// New comment
// =============================================================================
router.get("/new", middleware.isLoggedIn, function(req, res){
  Trip.findById(req.params.id, function(err, trip){
    if(err){
      console.log(err);
    } else {
      res.render("comments/new", {trip: trip});
    }
  });
});
// =============================================================================
// Create comment
// =============================================================================
router.post("/", middleware.isLoggedIn, function(req, res){
  Trip.findById(req.params.id, function(err, trip){
    if(err){
      console.log(err);
      res.redirect("/trips");
    } else {
      Comment.create(req.body.comment, function(err, comment){
        if(err){
          req.flash("error", "Something went wrong");
          console.log(err);
        } else {
          //add username
          comment.author.id = req.user._id;
          comment.author.username = req.user.username;

          //save comment
          comment.save();
          trip.comments.push(comment);
          trip.save();
          req.flash("success", "Comment added succesfully!");
          res.redirect("/trips/" + trip._id);
        }
      });
    }
  });
});
// =============================================================================
// Edit
// =============================================================================
router.get("/:comment_id/edit", middleware.checkCommentOwnership, function(req, res){
  Comment.findById(req.params.comment_id, function(err, foundComment){
    if(err){
      res.redirect("back");
    } else {
      res.render("comments/edit", {trip_id: req.params.id, comment: foundComment});
    }
  });
});
// =============================================================================
// Update
// =============================================================================
router.put("/:comment_id", middleware.checkCommentOwnership, function(req, res){
  Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function(err, updatedComment){
    if(err){
      res.redirect("back");
    } else {
      res.redirect("/trips/" + req.params.id);
    }
  });
});
// =============================================================================
// Destroy
// =============================================================================
router.delete("/:comment_id", middleware.checkCommentOwnership, function(req, res){
  Comment.findByIdAndRemove(req.params.comment_id, function(err){
    if(err){
      res.redirect("/trips");
    } else {
      Trip.findById(req.params.id, function(err, trip)  {
        if(err) {
          console.log(err);
        } else {
          // DELETE COMMENT FROM TRIP MODEL
          var allComments =  trip.comments;
          var index = allComments.indexOf(req.params.comment_id);
          allComments.splice(index, 1);
          trip.comments = allComments;
          trip.save();
          req.flash("success", "Comment succesfully deleted!");
          res.redirect("/trips/" + req.params.id);
        }
      });
    }
  });
});

module.exports = router;
